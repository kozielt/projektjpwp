package com.kozielt.webstore.domain.repository;

import com.kozielt.webstore.domain.Order;

public interface OrderRepository {

    Long saveOrder(Order order);
}
