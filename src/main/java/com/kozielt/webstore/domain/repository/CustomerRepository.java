package com.kozielt.webstore.domain.repository;

import com.kozielt.webstore.domain.Customer;

import java.util.List;

public interface CustomerRepository {

    List<Customer> getAllCustomers();
}
