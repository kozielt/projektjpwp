package com.kozielt.webstore.domain.repository.impl;

import com.kozielt.webstore.domain.Product;
import com.kozielt.webstore.domain.repository.ProductRepository;
import com.kozielt.webstore.exception.ProductNotFoundException;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private List<Product> productList = new ArrayList<Product>();

    @Override
    public List<Product> getAllProducts() {
        return productList;
    }

    @Override
    public Product getProductById(String productId) {

        Optional<Product> optional = productList.stream()
                .filter(p -> p.getProductId().equals(productId))
                .findAny();

        if(!optional.isPresent()){
            throw new ProductNotFoundException(productId);
        }

        return optional.get();
    }

    @Override
    public List<Product> getProductsByCategory(String category) {
        return productList.stream()
                .filter(p-> p.getCategory().equalsIgnoreCase(category))
                .collect(Collectors.toList());
    }

    public List<Product> getProductsByManufacturer(String manufacturer){
        return productList.stream()
                .filter(p-> p.getManufacturer().equalsIgnoreCase(manufacturer))
                .collect(Collectors.toList());
    }

    @Override
    public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams) {

        Set<Product> productsByBrand = new HashSet<>();
        Set<Product> productsByCategory = new HashSet<>();
        Set<String> criterias = filterParams.keySet();

        if(criterias.contains("brand")){
            for(String brandName: filterParams.get("brand")){
                productsByBrand.addAll(this.getProductsByManufacturer(brandName));
            }
        }

        if(criterias.contains("category")){
            for(String category : filterParams.get("category")){
                productsByCategory.addAll(this.getProductsByCategory(category));
            }
        }

        productsByCategory.retainAll(productsByBrand);
        return productsByCategory;
    }

    @Override
    public void addProduct(Product product) {
        productList.add(product);
    }

    //EFFECTS OF NON DB REPOSITORY
    public ProductRepositoryImpl(){
        initializeProducts();
    }

    private void initializeProducts(){
        Product iphone = new Product("P1234","iPhone 6s", new BigDecimal(3640));
        iphone.setDescription("Apple iPhone 6s, posiada 4,7 calowy ekran z wyświetlaczem Retina HD ");
        iphone.setCategory("Smartfon");
        iphone.setManufacturer("Apple");
        iphone.setUnitsInStock(1000);

        Product laptopDell = new Product("P1235","Dell Inspiron", new BigDecimal(2519));
        laptopDell.setDescription("Dell Inspiron, 15-calowy laptop (biały) z procesorem Intel Core i5-7200U");
        laptopDell.setCategory("Laptop");
        laptopDell.setManufacturer("Dell");
        laptopDell.setUnitsInStock(1000);

        Product tabletNexus = new Product("P1236","Nexus 7 II", new BigDecimal(969));
        tabletNexus.setDescription("Asus Nexus 7 jest 7-calowym tabletem z 4-rdzeniowym procesorem Qualcomm SnapdragonTM S4 Pro");
        tabletNexus.setCategory("Tablet");
        tabletNexus.setManufacturer("Asus");
        tabletNexus.setUnitsInStock(1090);

        Product huaweiPro = new Product("P1237", "Huawei Mate 9 Pro ", new BigDecimal(3699));
        huaweiPro.setDescription("Huawei Mate 9 Pro, posiada aż  6GB pamięci RAM oraz 128GB na dane");
        huaweiPro.setCategory("Smartfon");
        huaweiPro.setManufacturer("Huawei");
        huaweiPro.setUnitsInStock(100);

        Product zenbook = new Product("P1238", "ASUS ZenBook 3", new BigDecimal(5999));
        zenbook.setDescription("ASUS ZenBook 3 UX390UA, 13,3 calowy ultrabook wyposażony w procesor i7 siódmej generacji");
        zenbook.setCategory("Laptop");
        zenbook.setManufacturer("Asus");
        zenbook.setUnitsInStock(845);

        Product macbook = new Product("P1239", "Apple MacBook Pro 13''", new BigDecimal(5399));
        macbook.setDescription("Apple MacBook Pro 13'', posiada procesor i5 oraz wyświetlacz Retina");
        macbook.setCategory("Laptop");
        macbook.setDescription("Apple");
        macbook.setUnitsInStock(623);

        productList.add(iphone);
        productList.add(laptopDell);
        productList.add(tabletNexus);
        productList.add(huaweiPro);
        productList.add(zenbook);
        productList.add(macbook);
    }

}
