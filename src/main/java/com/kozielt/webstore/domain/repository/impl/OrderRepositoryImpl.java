package com.kozielt.webstore.domain.repository.impl;

import com.kozielt.webstore.domain.Order;
import com.kozielt.webstore.domain.repository.OrderRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class OrderRepositoryImpl implements OrderRepository {


    private Map<Long, Order> listOfOrders;
    private long nextOrderId;

    public OrderRepositoryImpl(){
        listOfOrders = new HashMap<>();
        nextOrderId = 1000;
    }

    @Override
    public Long saveOrder(Order order) {
        order.setOrderId(getNextOrderId());
        listOfOrders.put(order.getOrderId(), order);
        return order.getOrderId();
    }


    private synchronized long getNextOrderId() {
        return nextOrderId++;
    }
}
