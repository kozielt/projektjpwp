package com.kozielt.webstore.domain.repository.impl;

import com.kozielt.webstore.domain.Customer;
import com.kozielt.webstore.domain.repository.CustomerRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    List<Customer> customerList = new ArrayList<>();

    @Override
    public List<Customer> getAllCustomers() {
        return customerList;
    }

    public CustomerRepositoryImpl(){
        initializeCustomers();
    }

    private void initializeCustomers() {

    }
}
