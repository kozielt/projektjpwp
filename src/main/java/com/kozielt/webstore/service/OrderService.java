package com.kozielt.webstore.service;

import com.kozielt.webstore.domain.Order;

public interface OrderService {
    void processOrder(String productId, int count);
    Long saveOrder(Order order);
}
