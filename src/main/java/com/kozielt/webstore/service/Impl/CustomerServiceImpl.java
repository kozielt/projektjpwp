package com.kozielt.webstore.service.Impl;

import com.kozielt.webstore.domain.Customer;
import com.kozielt.webstore.domain.repository.CustomerRepository;
import com.kozielt.webstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }
}
