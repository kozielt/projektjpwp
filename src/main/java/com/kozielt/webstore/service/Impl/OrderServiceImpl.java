package com.kozielt.webstore.service.Impl;

import com.kozielt.webstore.domain.Order;
import com.kozielt.webstore.domain.Product;
import com.kozielt.webstore.domain.repository.OrderRepository;
import com.kozielt.webstore.domain.repository.ProductRepository;
import com.kozielt.webstore.service.CartService;
import com.kozielt.webstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CartService cartService;

    @Override
    public void processOrder(String productId, int count) {
        Product product = productRepository.getProductById(productId);

        if(product.getUnitsInStock() < count){
            throw new IllegalArgumentException("Za malo towaru. Obecna liczba w magazynie to: " + product.getUnitsInStock());
        }

        product.setUnitsInStock(product.getUnitsInStock() - count);
    }

    @Override
    public Long saveOrder(Order order) {
        Long orderId = orderRepository.saveOrder(order);
        cartService.delete(order.getCart().getCartId());
        return orderId;
    }
}
