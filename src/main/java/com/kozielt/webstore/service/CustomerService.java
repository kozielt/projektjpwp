package com.kozielt.webstore.service;

import com.kozielt.webstore.domain.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();
}
