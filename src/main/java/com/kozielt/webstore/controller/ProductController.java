package com.kozielt.webstore.controller;

import com.kozielt.webstore.domain.Product;
import com.kozielt.webstore.exception.NoProductFoundUnderCategoryException;
import com.kozielt.webstore.exception.ProductNotFoundException;
import com.kozielt.webstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/")
    public String home(Model model){
        return "redirect:/products";
    }

    @RequestMapping("/products")
    public String list(Model model){
        model.addAttribute("products", productService.getAllProducts());
        return "products";
    }

    @RequestMapping("/products/cat")
    public String getProductsByCategory(Model model, @RequestParam("cat") String category) {
        List<Product> products = productService.getProductsByCategory(category);
        if(products == null || products.isEmpty()){
            throw new NoProductFoundUnderCategoryException();
        }
        model.addAttribute("products", products);
        return "products";
    }

    @RequestMapping("/products/category")
    public String getProductsByCategoryA(Model model, @RequestParam("searchString") String category ){
        List<Product> products = productService.getProductsByCategory(category);
        if(products == null || products.isEmpty()){
            throw new NoProductFoundUnderCategoryException();
        }
        model.addAttribute("products", products);
        return "products";
    }

    @RequestMapping("/products/filter/{ByCriteria}")
    public String getProductsByFilter(@MatrixVariable(pathVar = "ByCriteria")Map<String, List<String>> filterParams, Model model){
        model.addAttribute("products", productService.getProductsByFilter(filterParams));
        return "products";
    }

    @RequestMapping("/products/product")
    public String getProductById(@RequestParam("id") String productId, Model model){
        model.addAttribute("product", productService.getProductById(productId));
        return "product";
    }

    @RequestMapping(value = "/products/add", method = RequestMethod.GET)
    public String getAddNewProductForm(Model model){
        Product newProduct = new Product();
        model.addAttribute("newProduct", newProduct);
        return "addProd";
    }

    @RequestMapping(value = "/products/add", method = RequestMethod.POST)
    public String processAddNewProductForm(@ModelAttribute("newProduct") @Valid Product product, BindingResult result, HttpServletRequest request){

        if(result.hasErrors()){
            return "addProd";
        }

        MultipartFile productImage = product.getProductImage();
        String rootDir = request.getSession().getServletContext().getRealPath("/");

        //TODO make it works ! Perhaps on windows it will work
        if(productImage != null && !productImage.isEmpty()){
            try {
                productImage.transferTo(new File(rootDir + "resources\\images\\" + product.getProductId() + ".jpg"));
            } catch (Exception e ){
                throw new RuntimeException("Niepowodzenie podczas proby zapisu obrazka", e);
            }
        }

        productService.addProduct(product);
        return "redirect:/products";
    }

    @RequestMapping("/products/invalidPromoCode")
    public String invalidPromoCode(){
        return "invalidPromoCode";
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ModelAndView handleError(HttpServletRequest request, ProductNotFoundException exception){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("invalidProductId", exception.getProductId());
        modelAndView.addObject("exception", exception);
        modelAndView.addObject("url", request.getRequestURL() + "?" + request.getQueryString());
        modelAndView.setViewName("productNotFound");
        return modelAndView;
    }

    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setAllowedFields("productId","name","unitPrice","description","manufacturer","category","unitsInStock", "condition","productImage");
    }
}
